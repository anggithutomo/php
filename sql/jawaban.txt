1. Membuat Database

create database myshop;

2. Membuat Table di Dalam Database

users
create table users(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id));

categories
create table categories(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> primary key(id));

items
create table items(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(10),
    -> primary key(id),
    -> category_id int(10),
    -> foreign key(category_id) references categories(id));

3. Memasukkan Data pada Table

users
insert into users(name,email,password) values
    -> ("John Doe","john@doe.com","john123"),
    -> ("Jane Doe","jane@doe.com","jenita123");

categories
insert into categories(name) values
    -> ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");

items
insert into items(name,description,price,stock,category_id) values
    -> ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
    -> ("Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Nebganvuk Data dari Database

users
select id,name,email from users;

items
select * from items where price>1000000;
select * from items where name like '%sang%';

categories
select items.id,items.name,items.description,items.price,
items.stock,items.category_id,categories.name from items 
inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database

update items set price=2500000 where name="Sumsang b50";